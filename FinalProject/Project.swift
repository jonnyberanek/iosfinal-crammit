//
//  Project.swift
//  FinalProject
//
//  Created by Jonny on 11/13/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import Foundation

class Project : Codable {
    
    public var tasks = [Task]()
    
    public var name : String
    
    init(){
        name = "unnamed project"
    }
    
    init(name: String){
        self.name = name
    }
    
    func getTaskOverallDuration(index: Int) -> TimeInterval{
        var x = index
        if(x >= tasks.count){x = tasks.count-1}
        var sum = 0.0
        for i in 0..<x{
            sum += tasks[i].remainingDuration
        }
        sum += tasks[x].remainingWorkDuration
        return sum
    }
    
    
}
