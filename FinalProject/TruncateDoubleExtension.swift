//
//  TruncateDoubleExtension.swift
//  FinalProject
//
//  Created by Jonny on 11/15/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import Foundation

//found at https://stackoverflow.com/questions/35946499/how-to-truncate-decimals-to-x-places-in-swift

extension Double
{
    func truncate(_ places:Int)-> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}
