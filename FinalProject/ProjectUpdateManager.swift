//
//  ProjectUpdateManager.swift
//  FinalProject
//
//  Created by Jonny on 12/18/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import Foundation
import UserNotifications

class ProjectUpdateManager : NSObject{
    
    var currentProjectsScreen : TasksTableViewController? = nil
    var countdownScreen : CountdownViewController? = nil
    
    public static let instance : ProjectUpdateManager = {
        let instance = ProjectUpdateManager()
        return instance
    }()
    
    override init(){
        super.init()
        ProjectsSingleton.instance.addObserver(self, forKeyPath: "currentProjectIndex", options: [.new], context: nil)
        
    }
    deinit {
        ProjectsSingleton.instance.removeObserver(self, forKeyPath: "currenProjectIndex") 
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        updateAll()
        
    }
    
    func updateAll(){
        countdownScreen?.completionTime = Date(timeIntervalSinceNow: ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0].duration ?? 0.0)
        currentProjectsScreen?.tableView.reloadData()
        
        ProjectUpdateManager.addNotification(type: ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0].type ?? .Break, timeInterval: ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0].duration ?? 1.0)
    }
    
    static func addNotification(type: Type, timeInterval: TimeInterval){
        let center = UNUserNotificationCenter.current()
        
        let note = UNMutableNotificationContent()
        
        var identifier = ""
        if(type == .Work){
            note.title = "Work session is over"
            note.body = "Tap to go to get your break!"
            identifier = "workSessionCompleted"
        }
        else if(type == .Break){
            note.title = "Break is over!"
            note.body = "Tap to go to get back to work!"
            identifier = "breakSessionCompleted"
        }
        else{
            note.title = ""
        }
        
        note.sound = UNNotificationSound.default
        note.categoryIdentifier = "sessionCompleted"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
        
        let request = UNNotificationRequest(identifier: "sessionCompleted", content: note, trigger: trigger)
        center.add(request, withCompletionHandler: {
            (error) in
            if error != nil {
                print("Something went very wrong")
            }
        })
    }
    
    func updateTasks(){
        
        if let project = ProjectsSingleton.instance.currentProject {
            project.tasks[0].sessions.removeFirst()
            print("session removed")
            if(project.tasks[0].sessions.isEmpty){
                print("task is completed")
                project.tasks.removeFirst()
                if(project.tasks.isEmpty){
                    print("project is completed")
                    ProjectsSingleton.instance.currentProjectIndex = -1
                }
            }
            if(!project.tasks.isEmpty){
                updateAll()
            }
        }
    }
    
    static func removeAllNotifications(){
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: ["sessionCompleted"])
    }
    
    
    
}
