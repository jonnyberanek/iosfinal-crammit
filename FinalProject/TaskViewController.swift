//
//  TaskViewController.swift
//  FinalProject
//
//  Created by Jonny on 11/14/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import UIKit

protocol TaskViewDelegate{
    func finished(child: TaskViewController)
}

//a composite of a view and a tableview, so lets hope for the best lol
class TaskViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var durationTextField: UITextField!
    @IBOutlet weak var breakDurTextField: UITextField!
    @IBOutlet weak var completionTimeLabel: UILabel!
    @IBOutlet weak var numBreaksTextField: UITextField!
    
    var taskIndex : Int = 0
    
    var currentTask : Task!
    
    var delegate : TaskViewDelegate? = nil
    
    @IBAction func saveTask(_ sender: Any) {
        
        ProjectsSingleton.instance.currentProject!.tasks[taskIndex].name = titleTextField.text!
        ProjectsSingleton.instance.currentProject!.tasks[taskIndex].description = descriptionTextField.text!
        
        ProjectsSingleton.instance.currentProject!.tasks[taskIndex].updateSessions(remainingWorkDuration: Double(durationTextField.text!)!, breakInMinutes: Double(breakDurTextField.text!)!, numBreaks: Int(numBreaksTextField.text!)!)
        
//
//        ProjectsSingleton.instance.currentProject!.tasks[taskIndex].totalWorkDuration = Double(durationTextField.text!)! * 60
//        if(Double(breakDurTextField.text!)! * 60 )
//        ProjectsSingleton.instance.currentProject!.tasks[taskIndex].breakDuration = Double(breakDurTextField.text!)! * 60
//        ProjectsSingleton.instance.currentProject!.tasks[taskIndex].numBreaks = Int(numBreaksTextField.text!)!
        
        delegate?.finished(child: self)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        descriptionTextField.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTextField.layer.borderWidth = 1
        
        titleTextField.text = currentTask.name
        descriptionTextField.text = currentTask.description
        durationTextField.text =  String((currentTask.remainingWorkDuration/60.0).truncate(2))
        breakDurTextField.text = String((currentTask.defaultBreakSessionDuration/60.0).truncate(2))
        completionTimeLabel.text = Formatters.time.string(from: Date(timeIntervalSinceNow: currentTask.remainingDuration))
        numBreaksTextField.text = String(currentTask.numBreaks)
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // todo: fix
        return currentTask.sessions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let session = currentTask.sessions[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sessionCell", for: indexPath) as! TaskComponentCell
        cell.timeLabel.text = Formatters.shortDuration.string(from: session.duration)
        
        if(session.type == .Work){ cell.typeLabel.text = "Work Session" }
        else if(session.type == .Break){ cell.typeLabel.text = "Break" }
        else { cell.typeLabel.text = "Error Session" }
        return cell
        
    }
        

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
