//
//  CountdownOperation.swift
//  FinalProject
//
//  Created by Jonny on 12/18/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import Foundation

class CountdownOperation : Operation{
    
    public static let countdownInstance : CountdownOperation = {
        let instance = CountdownOperation()
        return instance
    }()
    
    override init(){
        
    }
    
    override func main(){
        
    }
    
    func changeTime(){
        
    }
    
    
}
