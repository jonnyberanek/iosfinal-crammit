//
//  MainTabBarController.swift
//  FinalProject
//
//  Created by Jonny on 11/13/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) { }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting up right and left swipes
        //Right swipe
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        //Left swipe
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)

        
        //test code - to be removed
//        let testProject = Project(name:"iOS Test Project")
//        testProject.tasks.append(Task(name: "Start XCode", description: "A rather difficult task, often stops progress on the rest of the the project.", durationInMinutes: 20.0, breakInMinutes:20.0, numSessions: 2))
//        testProject.tasks.append(Task(name: "Cry", description: "Aaaaaaah", durationInMinutes: 60.0, breakInMinutes:20.0, numSessions: 3))
//        testProject.tasks.append(Task(name: "Ragequit XCode", description: "Nothing is working, give up now", durationInMinutes: 1.0, breakInMinutes:5.0, numSessions: 2))
//        ProjectsSingleton.instance.projects.append(testProject)
//        ProjectsSingleton.instance.currentProjectIndex = 0
//        ProjectsSingleton.instance.updateJson()
    }
    
    //Function that responds to the swipes and adjusts the view appropriately
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                self.selectedIndex -= 1
            case UISwipeGestureRecognizer.Direction.left:
                self.selectedIndex += 1
            default:
                break
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
