//
//  addProjectViewController.swift
//  FinalProject
//
//  Created by Gregory Oleynik on 12/17/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import UIKit

protocol addDelegate{
    func isfinished(child: addProjectViewController)
}

class addProjectViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, addProjDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var fld: UITextField!
    
    var delegate: addDelegate? = nil
    
    
    func finished(child: addTasksToNewViewController) {
        
        //appends task to task array
        tasks.append(child.task)
        child.dismiss(animated: true, completion: nil)
        tableView.reloadData()
    }
    
    //our array of tasks
    var tasks : [Task] = []
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell") as! TaskCell
        
        //Setting all of our labels to display information
        cell.nameLabel.text = tasks[indexPath.row].name
        cell.descLabel.text = tasks[indexPath.row].description
        cell.remainingDurationLabel.text = Formatters.shortDuration.string(from: tasks[indexPath.row].remainingWorkDuration)
        cell.estimatedCompletionLabel.text = Formatters.shortDuration.string(from: tasks[indexPath.row].remainingDuration)
        return cell
    }
    
    
    @IBAction func cancelButton(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)

        
    }
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) { }

    //Done adding tasks
    @IBAction func doneButton(_ sender: Any) {
        delegate?.isfinished(child: self)
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)

    }
    
    //Placeholder task
    var task: Task = Task()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //IFFY
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        //I'm not 100% sure how to do stuff like this, but this prevents the app
        //from crashing when we AREN'T moving forward (i.e. when we hit cancel)
        //Makes sure that when we hit the ADD button we still properly set up segue
        if sender is UIButton{
            let child = segue.destination as! addTasksToNewViewController
            child.delegate = self
        }
        
    }
        


        // Do any additional setup after loading the view.
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
