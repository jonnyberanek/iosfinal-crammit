//
//  ProjectsSingleton.swift
//  FinalProject
//
//  Created by Jonny on 11/13/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import Foundation


class ProjectsSingleton : NSObject {
    
    var saveProjects : [String]

    var projects = [Project]()
    var fileUrl : URL

    
    //@objc dynamic private var currentIndex : Int = -1
    @objc dynamic var currentProjectIndex : Int = -1 {
        willSet{
            if projects.indices.contains(newValue){
                self.currentProjectIndex = newValue
            }
            else{
                print("index not found");
                self.currentProjectIndex = -1
            }
        }
    }
    static let instance : ProjectsSingleton = {
        let instance = ProjectsSingleton()
        return instance
    }()
    
    var currentProject : Project? {
        get{
            if currentProjectIndex >= 0{
                return projects[currentProjectIndex]
            }
            return nil
        }
    }
    
    private override init(){
        
        saveProjects = [String]()
        print("hi")
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dirUrl = URL(fileURLWithPath: dirPath)

        fileUrl = dirUrl.appendingPathComponent("file.txt")
        fileUrl = URL(string: String(fileUrl.absoluteString.dropFirst(7)))!
        print(fileUrl)
        
        let fm = FileManager.default
        if fm.fileExists(atPath: fileUrl.absoluteString){
            do{
                let indata = try Data(contentsOf: fileUrl)
                print(indata)
                projects = try JSONSerialization.jsonObject(with: indata, options: .mutableLeaves) as! [Project]
                
            }catch{
                print("No file data")
            }
        }
        else{
            if (fm.createFile(atPath: fileUrl.absoluteString, contents: nil, attributes: nil)){
                print("crying")
            }
            else{
                print("crying more")
            }
        }
        
        super.init()
        //self.addObserver(self, forKeyPath: "saveProjects", options: [.new], context: nil)
        
    }
    
    func updateJson(){
//        do{
//            let json = try JSONSerialization.data(withJSONObject: projects, options: JSONSerialization.WritingOptions.prettyPrinted)
//            try json.write(to: fileUrl)
//        } catch{
//
//        }
    }
        
    
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//
//        do{
//            let json = try JSONSerialization.data(withJSONObject: saveProjects, options: JSONSerialization.WritingOptions.prettyPrinted)
//            try json.write(to: fileUrl)
//        } catch{
//
//        }
//    }
    
    deinit
    {
        //self.removeObserver(self, forKeyPath: "saveProjects")
        print("bye")
        do{
            let json = try JSONSerialization.data(withJSONObject: projects, options: JSONSerialization.WritingOptions.prettyPrinted)
            try json.write(to: fileUrl)
        } catch{
            
        }
    }

    
}
