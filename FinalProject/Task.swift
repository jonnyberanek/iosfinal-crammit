//
//  Task.swift
//  FinalProject
//
//  Created by Jonny on 11/13/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import Foundation

enum Type: Int, Codable {    
    case Work = 0
    case Break = 1
    
}

struct Session : Codable{
    
    var type : Type = .Work
    var duration : TimeInterval = 0.0
    
    init(type t: Type, duration d: Double) {
        type = t
        duration = d
    }

}

class Task : Codable {
    
    
    var name : String = "task"
    var description : String = "no description"

    var sessions = [Session]()
    
    var numBreaks : Int {
        get{ return sessions.count/2 }
    }
    
    private var totalWorkDuration : TimeInterval = 0.0
    //for a currently running task
    var remainingWorkDuration : TimeInterval {
        get{
            var sum  = 0.0
            for s in sessions{
                if(s.type == .Work) {sum += s.duration}
            }
            return sum
        }
    }
    var remainingDuration : TimeInterval {
        get{
            var sum  = 0.0
            for s in sessions{
                sum += s.duration
            }
            return sum
        }
    }
    
    private var defaultWorkSessionDuration : TimeInterval = 0.0 //is never used publicly, can change if necessary
    var defaultBreakSessionDuration : TimeInterval = 0.0

    //Default constructor
    init(){
        
    }
    
    init(name: String, description: String, durationInMinutes dur: Double){
        self.name = name
        self.description = description
        self.totalWorkDuration = dur*60
    }
    
    init(name: String, description: String, durationInMinutes dur: Double, breakInMinutes bDur: Double, numSessions: Int){
        self.name = name
        self.description = description
        self.totalWorkDuration = dur*60
        generateSessions(numSessions: numSessions, breakDurationInMinutes: bDur)
    }
    
    func generateSessions(numSessions: Int, breakDurationInMinutes breakDuration: Double, workDuration: Double = -1.0){
        
        if(workDuration < 0){
            defaultWorkSessionDuration = totalWorkDuration / Double(numSessions)
        }
        else{
            defaultWorkSessionDuration = workDuration / Double(numSessions)
        }
        defaultBreakSessionDuration = breakDuration*60
        
        for _ in 0..<numSessions-1{
            sessions.append(Session(type: .Work, duration: defaultWorkSessionDuration))
            sessions.append(Session(type: .Break, duration: defaultBreakSessionDuration))
        }
        sessions.append(Session(type: .Work, duration: defaultWorkSessionDuration))
    }
    
    func updateSessions(remainingWorkDuration: Double, breakInMinutes bDur: Double, numBreaks: Int){
        
        
        //check so this work doesnt have to be done on each save
        if(remainingWorkDuration*60 == self.remainingWorkDuration &&
            bDur*60 == defaultBreakSessionDuration &&
            numBreaks == self.numBreaks) {
            print("unchanged")
            return;
        }
        print("changed")
        
        //does this chunk make sense?
        var numSessions =  numBreaks + 1
        totalWorkDuration = remainingWorkDuration * 60
        var timeRemaining = totalWorkDuration
        
        
        let isInProgress = sessions[0].duration < defaultWorkSessionDuration - 1.0 //catches floating point errors, 1 seconds off is not crucial
        if(isInProgress && sessions[0].type == .Work){
            if(numSessions == 1){
                sessions[0].duration = timeRemaining
                return
            }
            timeRemaining -= sessions[0].duration
            numSessions -= 1
            for _ in 1..<sessions.count{
                sessions.removeLast()
            }
            sessions.append(Session(type: .Break, duration: bDur*60))
            generateSessions(numSessions: numSessions, breakDurationInMinutes: bDur, workDuration: timeRemaining)
        }
        else{
            sessions.removeAll()
            generateSessions(numSessions: numSessions, breakDurationInMinutes: bDur)
        }
        
    }

    
    
}

