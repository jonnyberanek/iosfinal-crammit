//
//  Formatters.swift
//  FinalProject
//
//  Created by Jonny on 11/15/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import Foundation

//holds all formatters for the project
class Formatters{
    
    //short time formatters XX:XX AM
    static let time: DateFormatter = {
        let tf = DateFormatter()
        tf.timeStyle = .short
        tf.dateStyle = .none
        return tf
    }()
    
    //short duration, abbreviated hours and minutes Xh Xm
    static let shortDuration: DateComponentsFormatter = {
        let cf = DateComponentsFormatter()
        cf.allowedUnits = [.hour, .minute]
        cf.unitsStyle = .abbreviated
        return cf
    }()
    
    //long duration, abbreivates Xd Xh Xm
    static let fullDuration : DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day, .hour, .minute, .second]
        formatter.unitsStyle = .abbreviated
        return formatter
    }()
}
