//
//  StringShortenExtension.swift
//  FinalProject
//
//  Created by Jonny on 12/18/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import Foundation

extension String{
    func toLengthOf(length:Int) -> String {
        if length <= 0 {
            return self
        } else if let to = self.index(self.startIndex, offsetBy: length, limitedBy: self.endIndex) {
            return self.substring(from: to)
            
        } else {
            return ""
        }
    }
}
