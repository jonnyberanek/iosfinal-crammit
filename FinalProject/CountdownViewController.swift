//
//  CountdownViewController.swift
//  FinalProject
//
//  Created by Gregory Oleynik on 11/14/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import UIKit


// I found a similar function on github that I had to modify to make the countdown work. I thought this was a better implementation becasue the countdown updates every second rather than the user pressing refresh.

class CountdownViewController: UIViewController {
    
    
    
    @IBOutlet weak var countdownLabel: UILabel!
    var releaseDate: Date?
    let queue = OperationQueue()

    
    var formatter = DateComponentsFormatter()
    
    var completionTime : Date!
    
    var countdownTimer : Timer!
    
    @IBAction func toggleTimer(_ sender: UIButton) {
        
        //Pause the timer
        if(sender.titleLabel!.text == "Pause"){
            countdownTimer.invalidate()
            sender.setTitle("Resume", for: .normal)
            sender.setTitleColor(UIColor.green, for: .normal)
            ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0].duration = completionTime.timeIntervalSinceNow
            ProjectUpdateManager.removeAllNotifications()
        }
            
        else{
            //Resume the timer
            completionTime  = Date(timeIntervalSinceNow: ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0].duration ?? 0.0)
            countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countDownDate), userInfo: nil, repeats: true)
            ProjectUpdateManager.addNotification(type: ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0].type ?? .Break, timeInterval: ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0].duration ?? 0.0)
            sender.setTitle("Pause", for: .normal)
            sender.setTitleColor(UIColor.red, for: .normal)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let releaseDateFormatter = DateFormatter()
        //releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //releaseDate = releaseDateFormatter.date(from: "2018-11-30 00:00:00")
        
        ProjectUpdateManager.instance.countdownScreen = self
        
        formatter.allowedUnits = [.day, .hour, .minute, .second]
        formatter.unitsStyle = .abbreviated
        
        queue.addOperation {
            self.completionTime = Date(timeIntervalSinceNow: ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0].duration ?? 0.0)
            
        }
        
        queue.waitUntilAllOperationsAreFinished()

        
        
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countDownDate), userInfo: nil, repeats: true)
    }
    
    @objc func countDownDate() {

        //let date = Date(timeInterval: -(ProjectsSingleton.instance.currentProject?.tasks[0].totalDuration ?? 0.0), since: Date())
        //print(date.description)
        
        //let calendar = Calendar.current
        
        //let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from:date)
        
        //let countdown = "Days \(diffDateComponents.day!),  Hours: \(diffDateComponents.hour!), Minutes: \(diffDateComponents.minute!), Seconds: \(diffDateComponents.second!)"
        //print(countdown)
        
        if(ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0] == nil){
            countdownLabel.text = "Project complete!"
            countdownTimer.invalidate()
        }
        
        countdownLabel.text! = formatter.string(from: Date(), to: completionTime)!
        ProjectsSingleton.instance.currentProject?.tasks[0].sessions[0].duration = completionTime.timeIntervalSinceNow
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

