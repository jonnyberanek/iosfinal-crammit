//
//  TasksTableViewController.swift
//  FinalProject
//
//  Created by Jonny on 11/13/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import UIKit

class TasksTableViewController: UITableViewController, TaskViewDelegate {
    
    var currentIndex : Int = 0
    
    var numRows : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProjectUpdateManager.instance.currentProjectsScreen = self
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func viewDidAppear(_ animated: Bool) {
        // to reload based on timer update
        if(ProjectsSingleton.instance.currentProject != nil && numRows > 0){
            print(ProjectsSingleton.instance.currentProjectIndex)
            tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        numRows = ProjectsSingleton.instance.currentProject?.tasks.count ?? 0
        return numRows
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TaskCell
        
        //Fill our cells with our task information
        if let project = ProjectsSingleton.instance.currentProject {
            let index = indexPath.row
            cell.nameLabel.text = project.tasks[index].name
            cell.descLabel.text = project.tasks[index].description
            cell.remainingDurationLabel.text = Formatters.shortDuration.string(from: project.tasks[index].remainingWorkDuration)
            cell.estimatedCompletionLabel.text = Formatters.time.string(from: Date(timeIntervalSinceNow: project.getTaskOverallDuration(index: index))) 
            
            
            
        }

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if let project = ProjectsSingleton.instance.currentProject{
            let child = segue.destination as! TaskViewController
            
            child.currentTask = project.tasks[currentIndex]
            child.taskIndex = currentIndex
            
            child.delegate = self
            
        }
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        currentIndex = indexPath.row
        return indexPath
    }
    
    func finished(child: TaskViewController) {
        
        child.dismiss(animated: true, completion: nil)
        
        print(child)
        tableView.reloadData()
    }
 

}
