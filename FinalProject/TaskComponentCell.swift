//
//  TaskComponentCell.swift
//  FinalProject
//
//  Created by Jonny on 11/15/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import UIKit

class TaskComponentCell: UITableViewCell {

    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
