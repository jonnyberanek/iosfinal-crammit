//
//  addTasksToNewViewController.swift
//  FinalProject
//
//  Created by Gregory Oleynik on 12/17/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import UIKit

protocol addProjDelegate{
    func finished(child: addTasksToNewViewController)
}

class addTasksToNewViewController: UIViewController {
    
    var delegate : addProjDelegate? = nil
    
    var task = Task()
    
    @IBOutlet weak var taskName: UITextField!
    
    @IBOutlet weak var taskDescription: UITextField!
    
    @IBOutlet weak var taskMinutes: UITextField!
    
    @IBOutlet weak var taskBreak: UITextField!
    
    @IBOutlet weak var taskBreakNumber: UITextField!
    
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButton(_ sender: Any) {
        
        task = Task(name: taskName.text ?? "unnamed", description: taskDescription.text ?? "no description", durationInMinutes: Double(taskMinutes.text!) ?? 1, breakInMinutes: Double(taskBreak.text!) ?? 1, numSessions: (Int(taskBreakNumber.text!) ?? 0)+1)
        
        //Finished with our child, ready to dismiss
        delegate?.finished(child: self)
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
      
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
