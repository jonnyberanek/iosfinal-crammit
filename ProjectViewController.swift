//
//  ProjectViewController.swift
//  FinalProject
//
//  Created by iOS dev on 12/18/18.
//  Copyright © 2018 Of Meese and Men. All rights reserved.
//

import UIKit

class ProjectViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, addDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var selectedRow = 0
    
    
    
    
    //NOTE: This can be replaced with any active/appwide array
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            print(ProjectsSingleton.instance.currentProjectIndex)
            
            if (ProjectsSingleton.instance.currentProjectIndex == (indexPath.row)){
                
                
                ProjectsSingleton.instance.currentProjectIndex = -1;
            
                

            }
            
            ProjectsSingleton.instance.projects.remove(at: Int(indexPath.row));
            tableView.reloadData();
            
            
            
            
        }
    }
    
    
    
    //Number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProjectsSingleton.instance.projects.count
    }
    
    //Build cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "projectCell") as! ProjectCell
        cell.title.text = ProjectsSingleton.instance.projects[indexPath.row].name
        return cell
    }
    
    //Child is finished, collect tasks and build project
    func isfinished(child: addProjectViewController) {
        //Append data/create new project
        let p = Project(name: child.fld.text ?? "no name")
        p.tasks = child.tasks
        //Add it to our project collection
        ProjectsSingleton.instance.projects.append(p)
        
        child.dismiss(animated: true, completion: nil)
        tableView.reloadData()
    }
    
    
    //Loading the project editor with our tasks
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        print("SENDER")
        if(sender is UIBarButtonItem){
            let child = segue.destination as! addProjectViewController
            child.delegate = self
            print("hello")
        }
        
        
        //Future implementation
        //child.tasks = projects[selectedRow].tasks
        //child.fld.text = projects[selectedRow].name
        
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?{
        selectedRow = indexPath.row
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ProjectsSingleton.instance.currentProjectIndex = indexPath.row
        //wow this is gross
//        ((self.tabBarController?.viewControllers?[1] as! UINavigationController).viewControllers[0] as! TasksTableViewController).tableView.reloadData()
        self.tabBarController?.selectedIndex = 1
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
